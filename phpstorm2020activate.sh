#!/bin/bash

cd ~/
path=".config/JetBrains"
phpStormDir=$(ls -at $path | grep 'PhpStorm*' | head -1)
cd $path/$phpStormDir
phpStormKeyFile=$(ls -at ./eval/ | grep 'PhpStorm*' | head -1)
rm "./eval/$phpStormKeyFile"
sed -i '/evlsprt/d' options/other.xml
cd ~/.java/.userPrefs/jetbrains
rm -rf phpstorm
echo
echo 'Success, if not - buy a license =) '
