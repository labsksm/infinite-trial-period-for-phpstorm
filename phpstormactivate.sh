#!/bin/bash

cd ~/
phpStormDir=$(ls -at | grep '.PhpStorm*' | head -1)
cd $phpStormDir
phpStormKeyFile=$(ls -at ./config/eval/ | grep 'PhpStorm*' | head -1)
rm "config/eval/$phpStormKeyFile"
sed -i '/evlsprt/d' config/options/other.xml
cd ~/.java/.userPrefs/jetbrains
rm -rf phpstorm
echo
echo 'Success, if not - buy a license =) '